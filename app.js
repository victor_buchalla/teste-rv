const express = require('express');
const path = require('path');
const app = express();
const products = require('./products.json');

app.use(express.static(path.join(__dirname, 'dist')));

app.get('/', (req, res) => {
  res.render('./dist/index.html');
});

app.get('/chairs', (req, res) => {
    res.send(products);
});

app.listen(process.env.PORT || 3030, process.env.IP || "127.0.0.1" ,() => {
  console.log(`app listening on port ${process.env.PORT || 3030} ip ${process.env.IP || "127.0.0.1"}`);
});
