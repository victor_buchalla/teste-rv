Documentação simples sobre as tecnologias, e instalação do projeto.

## Tecnologias
- [NPM Scripts](https://docs.npmjs.com/misc/scripts) - Build Scripts.
- [Pug](http://jade-lang.com/) - Template Engine.
- [Critical](https://github.com/addyosmani/critical) - Critical path css .
- [Stylus](http://stylus-lang.com/) - Preprocessador.
- [Rucksack](https://simplaio.github.io/rucksack/) - Postcss Features.
- [Jeet](http://jeet.gs/) - Grid System.
- [Rupture](http://jenius.github.io/rupture/) - Media Queries.
- [Webpack](https://webpack.github.io/) - Module Bundler.

## Instalação

Você precisará do [NodeJS](http://nodejs.org/) instalado em sua máquina.

```sh
# Clone o repositório.
$ git clone https://victor_buchalla@bitbucket.org/victor_buchalla/teste-rv.git project
$ cd project

# Inicie o servidor em node
$ node app.js
```

Não é necessário, mas caso você queira gerar os assets temos 2 tasks principais para isso

- `npm run build:dev` Gerar os arquivos html, css, js e imagens finais sem nenhum método de otimização

- `npm run build:prod` Gerar os arquivos html, css, js e imagens finais compactados e otimizados