import parallax from './ui-components/parallax.js';
import createCarousel from './ui-components/products-carousel.js';
import productsControlFactory from './ui-components/products-control.js';
import 'whatwg-fetch';
const armchairsCarousel = createCarousel(document.querySelector('[data-id="armchairs-carousel"]'));

const bestofCarousel = createCarousel(document.querySelector('[data-id="bestof-carousel"]'));

const fetch = window.fetch;

const productsControl = productsControlFactory({ 
    armchairsCarousel,
    bestofCarousel,
    fetch
});

const filtersWrapper = document.getElementById('products-filters');

productsControl(filtersWrapper);

parallax();
