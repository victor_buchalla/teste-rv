export default function(selector = '.parallax-container > .parallax') {
    
    const image = document.querySelector(selector);
    
    animate();
    
    function animate() {
        requestAnimationFrame(animate);
        updateImages(image);
    }
    
    function updateImages(image) {
        let { top: imageTop } = image.getBoundingClientRect();
        let { bottom: containerBottom } = image.parentNode.getBoundingClientRect();
        
        if (containerBottom < 0) {
            return;
        }
        
        if (imageTop < -1) {
            let y = (imageTop / 2) * -1;
            image.style.transform = `translate3d(0, ${y}px, 0)`;
        } else {
            image.style.transform = `translate3d(0, 0px, 0)`;
        }
    }
}
