import Flickity from 'flickity';

export default function createCarousel(element, extraOptions = {}) {
    
    const options = Object.assign({}, {
        prevNextButtons: false,
        contain: true,
        groupCells: '100%'
    }, extraOptions);
    
    const carousel = new Flickity(element, options);
    
    return carousel;
    
}