import { removeFromArray } from '../helpers/pure-functions.js';

let store = null;

function fetchProducts({ fetch }) {
    return fetch('/chairs')
        .then( response => response.json());
}

function buildProductItem(product) {
    return (
        `<div class="product-block">
            <div class="header">
                ${product.chairType} / ${product.material}
            </div>
            <div class="content">
                <img src="${product.picture}" width="200" height="200">
                <p class="description">
                    ${product.description}
                </p>
                <span>$ ${product.price}</span>
            </div>
            <div class="extra">
                <button class="btn-secondary -has-icon">
                    <svg class="icon">
                        <use xlink:href="#cart">
                    </svg>
                    Buy now
                </button>
            </div>
        </div>`
    );
}

function buildCarouselContent(carousel) {
    if (carousel.length > 0) {
        return carousel.map(buildProductItem).join('');
    } else {
        return "<div>No products found!</div>";
    }
}

function updateUI(
    { armchairsCarousel, bestofCarousel }, 
    filterWrapper, newState
) {
    let { products, chairType ,material } = newState;
    
    const filterType = (type) => (product) => (
        product.chairType === type &&
        material.indexOf(product.material) != -1 &&
        chairType.indexOf(product.chairType) != -1
    )
    
    let armchairProducts = products.filter( filterType("armchair") );
    
    let chairsProducts = products.filter( filterType("chair") );
    
    let chairs = buildCarouselContent(chairsProducts);
    bestofCarousel.slider.innerHTML = chairs;
    
    let armchairs = buildCarouselContent(armchairProducts);
    armchairsCarousel.slider.innerHTML = armchairs;
    
    [].concat(chairType, material).forEach((id) => {
        let checkbox = filterWrapper.querySelector(`#${id}`);
        checkbox.checked = true;
    });
    
    bestofCarousel.reloadCells();
    bestofCarousel.resize();
    armchairsCarousel.reloadCells();
    armchairsCarousel.resize();
}

function filterChairType(state, action) {
    let { chairType } = state;
    let newChairType;
    if (action.show === true) {
        newChairType = chairType.concat([action.chairType]);
    } else {
        let index = chairType.indexOf(action.chairType);
        newChairType = removeFromArray(chairType, index);
    }
    return Object.assign({}, state, {chairType: newChairType})
}

function filterMaterial(state, action) {
    let { material } = state;
    let newMaterial;
    if (action.show === true) {
        newMaterial = material.concat([action.material]);
    } else {
        let index = material.indexOf(action.material);
        newMaterial = removeFromArray(material, index);
    }
    return Object.assign({}, state, {material: newMaterial})
}

function filterProducts(state, action, render) {
    let newState;
    switch (action.type) {
        case 'FILTER_CHAIRTYPE':
            newState = filterChairType(state, action);
            break;
        case 'FILTER_MATERIAL': 
            newState = filterMaterial(state, action);
            break;
        case 'RESET_FILTERS':
            newState = Object.assign({}, state, {
                material: ['wood', 'foam'],
                chairType: ['chair', 'armchair']
            });
            break;
    }
    render(newState);
    store = newState;
}


function bindEvents(filtersWrapper, render) {
    const callers = filtersWrapper.querySelectorAll('[data-action]');
    
    const handleClick = (ev) => {
        let target = ev.target;
        let dataAction = JSON.parse(target.getAttribute('data-action'));
        let action = Object.assign({}, dataAction, {show: target.checked });
        filterProducts(store, action, render);
    }
    
    [].forEach.call(callers, (caller) => {
        caller.addEventListener('click', handleClick);
    });
}

function createInitialStore(response) {
    let newState = Object.assign({}, {
        products: response,
        material: ['wood', 'foam'],
        chairType: ['chair', 'armchair']
    });
    store = newState;
    return newState;
}

function productsControl(deps, filtersWrapper) {
    
    let promise = fetchProducts(deps);
    
    const render = updateUIFactory(deps, filtersWrapper);
    
    promise
        .then( response => createInitialStore(response))
        .then( newState => render(newState) )
        .then( () => bindEvents(filtersWrapper, render));
    
}

function updateUIFactory(deps, filtersWrapper) {
    return updateUI.bind(null, deps, filtersWrapper);
}


export default function productsControlFactory(deps) {
    return productsControl.bind(null, deps)
}